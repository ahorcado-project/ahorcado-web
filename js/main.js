"use strict";


const palabras = ["huron", "perro", "gato", "elefante", "cocodrilo", "gallina", "pollo"];

/***********************************************/

let vidas = 6;
let palabraOculta = "";
let palabraAdivinar = "";



/***********************************************/

const btnCrearPalabra = document.getElementById("crearPalabra")
const btnComprueba = document.getElementById("btnComprueba");
const vidasouput = document.getElementById("Vidas");
const output = document.getElementById("output");
const seccionJuego = document.getElementById("seccionJuego");
const hasGanado = document.getElementById("hasGanado");
const hasperdido = document.getElementById("hasPerdido");
const resetWin = document.getElementById("resetWin");
const resetLose = document.getElementById("resetLose");
const inputLetra = document.getElementById("inputLetra");
const compruebaBlock = document.getElementsByClassName("compruebaBlock");

/************************************************/

function crearPalabra(){

vidas = 6;
palabraOculta = "";
palabraAdivinar = "";
vidasouput.innerHTML = vidas;

hasperdido.style.display = "none";
hasGanado.style.display = "none";
seccionJuego.style.display = "flex";
btnComprueba.style.display = "block";
inputLetra.disabled = false;

palabraOculta = palabras[ Math.floor(Math.random()*palabras.length) ];
console.log(palabraOculta);

for(let i = 0; i<palabraOculta.length; i++){
    palabraAdivinar = palabraAdivinar + "_ ";

    output.innerHTML = palabraAdivinar;
}
    inputLetra.focus();
    btnCrearPalabra.disabled = true;
    btnComprueba.disabled = true;

}


btnCrearPalabra.addEventListener("click", crearPalabra)


/*********************************************/

function comprueba() {
    
    let input = inputLetra.value.toLowerCase();
    palabraOculta = palabraOculta.toLowerCase();
    let resultado = "";

    for(let i = 0; i < palabraOculta.length; i++){
        
        if(input == palabraOculta[i]){
            resultado = resultado + input + " ";
            
        }else{
            resultado = resultado + palabraAdivinar[i*2] + " ";
        }
        
    }

    if(resultado === palabraAdivinar){
        vidas--;
        vidasouput.innerHTML = vidas;  
    }

    palabraAdivinar = resultado;
    output.innerHTML = palabraAdivinar;
    inputLetra.value = "";
    inputLetra.focus();
    btnComprueba.disabled = true;

}

btnComprueba.addEventListener("click", comprueba);


/**************************************************/

inputLetra.addEventListener("keypress", (event) =>{
    if(event){
        btnComprueba.disabled = false;
    }else{
        btnComprueba.disabled = true;
    }


});

/******************************************************/

function ganasOPierdes() {

    if(vidas === 0){
        seccionJuego.style.display = "none";
        hasperdido.style.display = "flex";
        btnCrearPalabra.disabled = false;
    }
    
    if(palabraAdivinar.search("_") === -1){
        seccionJuego.style.display = "none";
        hasGanado.style.display = "flex"
        btnCrearPalabra.disabled = false;
    }
    

}


btnComprueba.addEventListener("click", ganasOPierdes);

/****************************************************/


